# git-api-scrapping


## args.json

 "url" : Git URL

 "authkey": Private key for git project

 "project": <username>/<repository_name>

 "json_file_name": file name with full path to read from git repo


## Run the script

Update the args.json

Run the ```read_file.py``` 


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/kk200488/git-api-scrapping.git
git branch -M main
git push -uf origin main
```
