import sys, os, urllib3, argparse, pdb
from urllib.parse import quote

# Silence the  insecure warnings.
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import gitlab
import json


class PythonGitlabProject():
    def __init__(self, url, authkey, project, json_file_name):
        # Parse command line arguments
        self.url = url
        self.authkey = authkey
        self.project_name = project
        self.file_name = json_file_name
        # self.file_name = quote(json_file_name, safe='')

        # Create python-gitlab server instance
        server = gitlab.Gitlab(self.url, data["authkey"], api_version=4, ssl_verify=False)
        # Get an instance of the project and store it off
        self.project = server.projects.get(self.project_name)

    def read_raw_file(self):
        print(self.project.files.raw(self.file_name,"master"))


if __name__ == '__main__':
    args_file = open('args.json')
    data = json.load(args_file)
    args_file.close()

    url = data["url"]
    authkey = data["authkey"]
    project = data["project"]
    json_file_name = data["json_file_name"]
    PythonGitlabProject(url=url, authkey=authkey, project=project, json_file_name=json_file_name).read_raw_file()
